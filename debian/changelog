pythondialog (3.5.1-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 01 Jun 2023 20:11:48 +0000

pythondialog (3.5.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-sphinx.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 01:04:15 +0100

pythondialog (3.5.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 16:09:33 -0400

pythondialog (3.5.1-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 20:37:59 +0000

pythondialog (3.5.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Re-export upstream signing key without extra signatures.

  [ Tristan Seligmann ]
  * New upstream release.
  * Add missing build-dep on python3-setuptools.
  * Set python3-dialog M-A: foreign (Closes: #918803).
  * Bump debhelper-compat to 13.
  * Bump Standards-Version to 4.5.0 (no changes).
  * Switch to dh-sequence-*.
  * Apply sphinxdoc:Built-Using.
  * Declare rootless build.
  * Add autodep8 tests.

 -- Tristan Seligmann <mithrandi@debian.org>  Tue, 21 Jul 2020 12:27:15 +0200

pythondialog (3.4.0-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:41:34 +0000

pythondialog (3.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Tristan Seligmann ]
  * New upstream release.
  * Bump Standards-Version to 3.9.8 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Thu, 23 Jun 2016 23:01:53 +0200

pythondialog (3.3.0-1) unstable; urgency=medium

  [ Florent Rougon ]
  * New upstream release.

 -- Tristan Seligmann <mithrandi@debian.org>  Tue, 02 Jun 2015 03:24:08 +0200

pythondialog (3.2.2-1) unstable; urgency=low

  [ Florent Rougon ]
  * New upstream release.

  [ Tristan Seligmann ]
  * Switch debian/watch to pypi.debian.net redirector.

 -- Florent Rougon <f.rougon@free.fr>  Tue, 05 May 2015 17:04:28 +0200

pythondialog (3.2.1-1) experimental; urgency=low

  [ Florent Rougon ]
  * Split python3-dialog.doc-base into python3-dialog.doc-base.manual and
    python3-dialog.doc-base.readme; otherwise, it appears as if README.rst
    were a text version of the pythondialog manual.

  [ Tristan Seligmann ]
  * New upstream release.
    - Remove add-glossary-label patch which was merged upstream.

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 08 Dec 2014 15:06:15 +0200

pythondialog (3.2.0-2) unstable; urgency=low

  * Add missing file/license for doc/intro/example.py to debian/copyright
    (closes: #766041).

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 20 Oct 2014 16:36:40 +0200

pythondialog (3.2.0-1) unstable; urgency=low

  [ Florent Rougon ]
  * New upstream release.

  * Add Build-Depends on python3-docutils,
    python3-sphinx (>= 1.0.7+dfsg-1~) and python3-pygments because this
    release brings a new Sphinx-based documentation.

  * add-glossary-label: new patch to add a "glossary" label right before
    the Glossary (maybe upstream bug, but upstream-installed Sphinx didn't
    complain about the missing label!).

  * Build-Depend on python3-doc so that intersphinx can use
    /usr/share/doc/python3/html/objects.inv (since downloading is
    prohibited during package build).
  * use-local-python3-doc-for-cross-references: new patch that:
      - uses /usr/share/doc/python3/html/objects.inv as the inventory file
        for Python cross-references;
      - resolves these references locally (with a
        file:///usr/share/doc/python3/html prefix)
  * Add a Suggests: python3-doc for these references to the local Python
    documentation.

  * Add python3-dialog.doc-base.

  [ Tristan Seligmann ]
  * Import packaging work by Florent Rougon.
    - python-dialog package removed, python3-dialog package added, since only
      Python 3 is supported upstream.
    - Reworked packaging fixes old bugs (closes: #608408, #615432, #646257)
  * Adopt package on behalf of DPMT (closes: #673962).
  * Add VCS control fields.
  * Check PGP signature in debian/watch.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 19 Oct 2014 12:46:15 +0200

pythondialog (2.7-1) unstable; urgency=low

  * Initial release (Closes: #402453).

 -- Adam Cécile (Le_Vert) <gandalf@le-vert.net>  Sun, 10 Dec 2006 16:30:10 +0100
